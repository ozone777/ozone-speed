<?php
   /*
   Plugin Name: Ozone Speed
   Plugin URI: https://ozoonegroup.co
   description: Create speed for search engines
   Version: 1.0
   Author: Pedro consuegra
   Author URI: https://ozoonegroup.co
   License: GPL2
   */
   
   if ( ! function_exists('write_log')) {
      function write_log ( $log )  {
         if ( is_array( $log ) || is_object( $log ) ) {
            error_log( print_r( $log, true ) );
         } else {
            error_log( $log );
         }
      }
   }

   function testing_log(){
   	write_log("Peter is here!!!!");
   }
   
   add_action('init', 'testing_log');
   
   
   add_action( 'wp_print_styles',     'my_deregister_styles', 100 );

   function my_deregister_styles()    { 
      
      wp_deregister_style( 'dashicons' ); 
	  wp_deregister_style( 'fontawesome' ); 
	  wp_deregister_style( 'open-sans' ); 
	  wp_deregister_style( 'et-gf-open-sans' ); 
	  

   }
   
   function print_footer_styles() {
	   
      $style_handle  = 'dashicons';
      wp_register_style( $style_handle, "/wp-includes/css/$style_handle".".css" );
      wp_print_styles( $style_handle );
	  
      $style_handle  = 'fontawesome';
      wp_register_style( $style_handle, "http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css" );
      wp_print_styles( $style_handle );
	 
   }
   
   add_action( 'wp_footer', 'print_footer_styles' );
   
   add_action('wp_head', function () {
     global $wp_scripts;

     foreach ($wp_scripts->queue as $handle) {
       $script = $wp_scripts->registered[$handle];
    
       //-- If version is set, append to end of source.
       $source = $script->src . ($script->ver ? "?ver={$script->ver}" : "");

       //-- Spit out the tag.
       echo "<link rel='preload' href='{$source}' as='script'/>\n";
     }
   }, 1);
   
   add_filter('autoptimize_filter_extra_gfont_fontstring','add_display'); function add_display($in) { return $in.'&display=swap'; }
   
   function remove_head_scripts() { 
	   
      remove_action('wp_head', 'wp_print_scripts'); 
      remove_action('wp_head', 'wp_print_head_scripts', 9); 
      remove_action('wp_head', 'wp_enqueue_scripts', 1);

      add_action('wp_footer', 'wp_print_scripts', 5);
      add_action('wp_footer', 'wp_enqueue_scripts', 5);
      add_action('wp_footer', 'wp_print_head_scripts', 5); 
   } 
   
  // add_action( 'wp_enqueue_scripts', 'remove_head_scripts' );
   
   /*
   function use_jquery_from_google () {
	   
   	if (is_admin()) {
   		return;
   	}

   	global $wp_scripts;
   	if (isset($wp_scripts->registered['jquery']->ver)) {
   		$ver = $wp_scripts->registered['jquery']->ver;
                   $ver = str_replace("-wp", "", $ver);
   	} else {
   		$ver = '1.12.4';
   	}

   	wp_deregister_script('jquery');
   	wp_register_script('jquery', "//ajax.googleapis.com/ajax/libs/jquery/$ver/jquery.min.js", false, $ver);
   }
   */
   
?>